package com.gz.controller;

import com.gz.common.SystemService;
import com.gz.common.model.Xiaochengxu;
import com.gz.utils.Response;
import com.jfinal.core.Controller;

public class XiaochengxuController extends Controller {
    public void get(){
        Xiaochengxu xiaochengxu= SystemService.getService().getXiaochengxu();
        renderJson(Response.responseJson(0,"success",xiaochengxu));
    }
    public void modify(){
        Xiaochengxu xiaochengxu=getBean(Xiaochengxu.class,"");
        if(xiaochengxu.getId()!=null){
            xiaochengxu.update();
        }else {
            xiaochengxu.save();
        }
        renderJson(Response.responseJson(0,"success",xiaochengxu));
    }
}
